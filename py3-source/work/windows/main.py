from tkinter import Label
from tkinter import  Entry
from tkinter import Tk
from tkinter import Button
from tkinter import scrolledtext
from tkinter.ttk import Combobox
from tkinter.ttk import Checkbutton
from tkinter.ttk import Radiobutton
from tkinter import messagebox

window = Tk()
window.title("Welcome to LikeGeeks app")
window.geometry("1000x2000")

lbl = Label(window,text="I'm a label",font=("Arial Bold",50))
lbl.grid(column=0,row=0)

txt = Entry(window,width=10)
txt.grid(column=0,row=2)
txt.focus()


def clicked():
    messagebox.showinfo("Message title","Message content")

chk = Checkbutton(window,text='Choose')
chk.grid(column=0,row=4)

btn = Button(window,text="Click Me",bg="orange",fg="red",command=clicked)
btn.grid(column=0,row=1)


combo = Combobox(window)
combo.grid(column=0,row=3)
combo['values'] = (1,2,3,4,5,"Text")
combo.current(1)

rad1 = Radiobutton(window,text="First",value=1)
rad1.grid(column=0,row=5)

s_txt = scrolledtext.ScrolledText(window,width=40,height=10)
s_txt.grid(column=0,row=6)
# s_txt.insert(INSERT,"You text goes here")

window.mainloop()