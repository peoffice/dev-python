# deal with excel library
import xlrd
import xlwt

import sys
import os


def get_classifications(file,sheet_name,class_name):
    """
    get all path items from excel file by class_name
    :param file: excel file
    :param sheet_name: sheet name
    :param class_name: classification name
    :return path items list
    """
    paths_list = []
    if os.path.isfile(file):
        book = xlrd.open_workbook(file)
        sheet = book.sheet_by_name(sheet_name)
        count = sheet.nrows
        for i in range(1,count-1):
            if sheet.cell(i,1).value==class_name:
                paths_list.append(sheet.cell(i,0).value)
    return paths_list


def write_paths(file,sheet_name,class_name,paths):
    if os.path.isfile(file):
        os.remove(file)

    book = xlwt.Workbook();
    sheet = book.add_sheet(sheet_name)
    sheet.write(0,0,"PATH")
    sheet.write(0,1,"CLASSIFICATION")
    sheet.write(0,2,"REMARK")
    number = 1;
    for path in paths:
        sheet.write(number,0,path)
        number += 1

    book.save(file)


def copy_dir(source_dir,target_dir):
    """
    copy all files from source dir to target dir
    :param source_dir: source dir
    :param target_dir: target dir
    :return:
    """
    if os.path.isfile(source_dir):
        (file_path, file_name) = os.path.split(target_dir)
        if not os.path.exists(file_path):
            os.makedirs(file_path)
        open(target_dir, "wb").write(open(source_dir, "rb").read())
    elif os.path.isdir(source_dir):
        children = os.listdir(source_dir)
        for child in children:
            sub_source_dir = os.path.join(source_dir,child)
            sub_target_dir = os.path.join(target_dir,child)
            if os.path.isfile(sub_source_dir):
                if not os.path.exists(target_dir):
                    os.makedirs(target_dir)
                if not os.path.exists(sub_target_dir) or (
                        os.path.exists(sub_target_dir) and (os.path.getsize(target_dir) != os.path.getsize(source_dir))):
                    open(target_dir, "wb").write(open(source_dir, "rb").read())
            else:
                copy_dir(sub_source_dir,sub_target_dir)


def remove_dir(source_dir):
    """
    remove source dir from disk
    :param source_dir: source dir need to be removed
    :return:
    """
    if os.path.isfile(source_dir):
        os.remove(source_dir)
    elif os.path.isdir(source_dir):
        children = os.listdir(source_dir)
        for child in children:
            sub_source_dir = os.path.join(source_dir,child)
            remove_dir(sub_source_dir)
        os.removedirs(source_dir)


def get_files(source_dir,files):
    if os.path.isfile(source_dir):
        files.append(source_dir)
    else:
        children = os.listdir(source_dir)
        for child in children:
            child_dir = os.path.join(source_dir,child)
            get_files(child_dir,files)




if __name__=="__main__":
    print("I'm in main function")
    # path = os.getcwd()
    # file = os.path.join(path,"dspe_git_structure_classification.xlsx")
    # list1 = get_classifications(file,"Source","ProdDesign")
    # list2 = get_classifications(file,"Config","ProdDesign")
    #
    # print(list1)
    # print(list2)
    #
    # dict1 = {}
    # dict1["Source"] = list1
    # dict1["Config"] = list2
    # print(dict1)
    #
    # source_dir = file
    # target_dir = "d:\\test2\\ttt.xlsx"
    # copy_dir(source_dir,target_dir)

    # remove_dir("d:\\test2")

    # source_dir = "D:\\@Job\\DSPE_CODE\\DEV\\Source"
    # files = []
    # get_files(source_dir,files)
    # file = "D:\\@Job\\DSPE_CODE\\DEV\\result.xlsx"
    # write_paths(file,"Source","All",files)

