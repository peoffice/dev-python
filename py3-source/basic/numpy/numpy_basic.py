import numpy as np


# print(np.eye(10))

# a = np.array(object=[1,2,3])
# print(a)

# dt = np.dtype([('age',np.int8)])
# a = np.array([(10,),(20,),(30,)],dtype=dt)
# print(a)
# print(a['age'])

student = np.dtype([('name','S20'),('age','i1'),('marks','f4')])
a = np.array([('abc',21,50),('xyz',18,75)],dtype=student)
print(a)
print(a.flags)


