import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


"""
programmer_1:制作箱线图
data.boxplot --> 数据转换为箱线图的字典格式
plt.annotate --> 绘图

programmer_2: 计算数据
range --> 极差
var --> 方差
dis --> 四分距

programmer_3: 画出盈利图（比例和数值）

programmer_4: 计算成对相关性
data.corr() --> dataframe中相互之间的相关性
data.corr()[u'百合酱蒸风爪'] --> dataframe某一项与其他项的相关性
"""

def programmer_1(file_name):
    catering_sale = file_name
    data = pd.read_excel(catering_sale,index_col='日期')
    #print(data)
    plt.figure()

    #画箱线图
    p = data.boxplot(return_type='dict')
    x = p['fliers'][0].get_xdata()
    y = p['fliers'][0].get_ydata().sort()
    print(x)
    print(y)
    for i in range(len(x)):
        # 处理临界情况，i=0时
        temp = y[i] - y[i-1] if i != 0 else -78/3
        plt.annotate(y[i],xy=(x[i],y[i]),xytext=(x[i]+0.05-0.8/temp,y[i]))
    plt.show()

if __name__ == "__main__":
    path = os.getcwd()
    programmer_1(path+'/data/catering_sale.xls')
    pass

