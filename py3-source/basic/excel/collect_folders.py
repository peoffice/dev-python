import os
import sys
import xlwt


def collect_folders(path,isRoot,list):
    if os.path.isdir(path):
        childs = os.listdir(path)
        isTargetNode = False
        for child in childs:
            child_path = os.path.join(path,child)
            if os.path.isfile(child_path):
                isTargetNode = True
                break
        if not isTargetNode:
            for child in childs:
                child_path = os.path.join(path,child)
                if os.path.isdir(child_path):
                    collect_folders(child_path,False,list)
        if isTargetNode and (not isRoot):
            list.append(path)


def write_excel(list,file):
    if os.path.isfile(file):
        os.remove(file)

    xls = xlwt.Workbook()
    sheet = xls.add_sheet("Source")
    sheet.write(0,0,"Item")
    sheet.write(0,1,"Category")

    index = 1
    for item in list:
        sheet.write(index,0,item)
        index += 1

    xls.save(file)


if __name__ == "__main__":
    #path = os.getcwd()
    if sys.argv.__len__() == 2:
        print(sys.argv[1])
        path = sys.argv[1]
        list = []
        collect_folders(path,False,list)
        file = os.path.join(path,"Source.xls")
        write_excel(list,file)
        print(list)
    pass
