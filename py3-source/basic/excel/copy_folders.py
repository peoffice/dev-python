import os

def copyFiles(sourceDir,targetDir):
    childs = os.listdir(sourceDir)
    for child in childs:
        source_file = os.path.join(sourceDir,child)
        target_file = os.path.join(targetDir,child)

        if os.path.isfile(source_file):
            # create director
            if not os.path.exists(targetDir):
                os.makedirs(targetDir)


            if not os.path.exists(target_file) or (os.path.exists(target_file) and (os.path.getsize(target_file)!=os.path.getsize(source_file))):
                open(target_file,"wb").write(open(source_file,"rb").read())

        else:
            copyFiles(source_file,target_file)

if __name__ == "__main__":
    sourceDir = "D:\\@GitLab\\dev-python\\py3-source"
    targetDir = "D:\\test"
    copyFiles(sourceDir,targetDir)
