import xml.sax

class MovieHandler(xml.sax.ContentHandler):
    def __init__(self):
        self.currentData = ""
        self.type = ""
        self.format = ""
        self.year = ""
        self.rating = ""
        self.starts = ""
        self.description = ""

    # 元素开始调用
    def startElement(self,tag,attribute):
        self.currentData = tag
        if tag == "movie":
            print("******Movie******")
            title = attribute["title"]
            print("Title:",title)


    # 元素调用结束
    def endElement(self,tag):
        if self.currentData == "type":
            print("Type:",self.type)
        elif self.currentData == "format":
            print("Format:",self.format)
        elif self.currentData == "year":
            print("Year:",self.year)
        elif self.currentData == "rating":
            print("Rating:",self.rating)
        elif self.currentData == "stars":
            print("Starts:,",self.starts)
        elif self.currentData == "description":
            print("Descrption,",self.description)

    # 读取字符时调用
    def characters(self, content):
        if self.currentData == "type":
            self.type = content
        elif self.currentData == "format":
            self.format = content
        elif self.currentData == "year":
            self.year = content
        elif self.currentData == "rating":
            self.rating = content
        elif self.currentData == "stars":
            self.stars = content
        elif self.currentData == "description":
            self.description = content


if __name__ == "__main__":
    # 创建一个 XMLReader
    parser = xml.sax.make_parser()
    # 关闭命名空间
    parser.setFeature(xml.sax.handler.feature_namespaces, 0)

    # 重写 ContextHandler
    Handler = MovieHandler()
    parser.setContentHandler(Handler)

    parser.parse("movies.xml")