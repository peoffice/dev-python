import os,sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


# read the CSV file
drinks = pd.read_csv('drinks.csv')
# print("type(drinks)")
# print(type(drinks))

# examine the data
# print("drinks.head")
# print(drinks.head())
# print("drinks.tail")
# print(drinks.tail())
# print("drinks.describe")
# print(drinks.describe())
# print("drinks.info")
# print(drinks.info())

# find missing values in a DataFrame
# print("drinks.isnull")
# print(drinks.isnull())
# print("drinks.isnull.sum")
# print(drinks.isnull().sum())


# selecting a column
# print(drinks['continent'])
# print(drinks.continent.describe())
# print(drinks.continent.value_counts())

# bar plot of number of countries in each continent
# drinks.continent.value_counts().plot(kind='bar',title='Countries per Continent')
# plt.xlabel('Continent')
# plt.ylabel('Count')
# plt.show()

# bar plot of average number of beer servings by continent
# drinks.groupby('continent').beer_servings.mean().plot(kind='bar')
# plt.show()



# histogram of beer servings
# drinks.beer_servings.hist(bins=20)

# grouped histogram of beer servings
# drinks.beer_servings.hist(by=drinks.continent, sharex=True)

# density plot of beer servings
# drinks.beer_servings.plot(kind='density', xlim=(0,500))

# boxplot of beer servings by continent
# drinks.boxplot(column='beer_servings', by='continent')

# scatterplot of beer servings versus wine servings
# drinks.plot(x='beer_servings', y='wine_servings', kind='scatter', alpha=0.3)

# same scatterplot, except all European countries are colored red
# colors = np.where(drinks.continent=='EU', 'r', 'b')
# drinks.plot(x='beer_servings', y='wine_servings', kind='scatter', c=colors)


'''
Joining Data
'''

# read 'u.data' into 'ratings'
# r_cols = ['user_id', 'movie_id', 'rating', 'unix_timestamp']
# ratings = pd.read_table('u.data', header=None, names=r_cols, sep='\t')

# read 'u.item' into 'movies'
# m_cols = ['movie_id', 'title']
# movies = pd.read_table('u.item', header=None, names=m_cols, sep='|', usecols=[0,1])

# merge 'movies' and 'ratings' (inner join on 'movie_id')
# movies.head()
# ratings.head()
movie_ratings = pd.merge(movies, ratings)
movie_ratings.head()


'''
Further Exploration
'''

# for each movie, count number of ratings
# movie_ratings.title.value_counts()

# for each movie, calculate mean rating
# movie_ratings.groupby('title').rating.mean().order(ascending=False)

# for each movie, count number of ratings and calculate mean rating
# movie_ratings.groupby('title').rating.count()
# movie_ratings.groupby('title').rating.mean()
# movie_stats = movie_ratings.groupby('title').agg({'rating': [np.size, np.mean]})
# movie_stats.head()  # hierarchical index

# limit results to movies with more than 100 ratings
# movie_stats[movie_stats.rating.size > 100].sort_index(by=('rating', 'mean'))

