#### 数据类型
* bool_: 布尔型数据类型
* int_: 默认的数据类型（类型于C语言中的long,int32或int64）
* intc: 与C的int类型一样，一般是int32或int64
* intp: 用于索引的整数类型（类似于 C 的 ssize_t，一般情况下仍然是 int32 或 int64）
* int8: 字节（-128 to 127）
* int16: 整数（-32768 to 32767）
* int32: 整数（-2147483648 to 2147483647）
* int64: 整数（-9223372036854775808 to 9223372036854775807）
* uint8: 无符号整数（0 to 255）
* uint16: 无符号整数（0 to 65535）
* uint32: 无符号整数（0 to 4294967295）
* uint64: 无符号整数（0 to 18446744073709551615）
* float_: float64 类型的简写
* float16: 半精度浮点数，包括：1 个符号位，5 个指数位，10 个尾数位
* float32: 单精度浮点数，包括：1 个符号位，8 个指数位，23 个尾数位
* float64: 双精度浮点数，包括：1 个符号位，11 个指数位，52 个尾数位
* complex_: complex128 类型的简写，即 128 位复数
* complex64: 复数，表示双 32 位浮点数（实数部分和虚数部分）
* complex128: 复数，表示双 64 位浮点数（实数部分和虚数部分）

#### 内建类型都有一个唯一定义它的字符代码
* b: 布尔
* i: （有符号）整型
* u: 无符号整型integer
* f: 浮点型
* c: 复数浮点型
* m: timedelta（时间间隔）
* M: datetime（日期时间）
* O: （Python）对象
* S,a: （byte-）字符串
* U: Unicode
* V: 原始数据（void)

#### Numpy数组属性
* ndarray.ndim: 秩，即轴的数量或维度的数量
* ndarray.shape: 数组的维度，对于矩阵，n 行 m 列
* ndarray.size: 数组元素的总个数，相当于 .shape 中 n*m 的值
* ndarray.dtype: ndarray 对象的元素类型
* ndarray.itemsize: ndarray 对象中每个元素的大小，以字节为单位
* ndarray.flags: ndarray 对象的内存信息
  * C_CONTIGUOUS (C): 数据是在一个单一的C风格的连续段中
  * F_CONTIGUOUS (F): 数据是在一个单一的Fortran风格的连续段中
  * OWNDATA (O): 数组拥有它所使用的内存或从另一个对象中借用它
  * WRITEABLE (W): 数据区域可以被写入，将该值设置为 False，则数据为只读
  * ALIGNED (A): 数据和所有元素都适当地对齐到硬件上
  * UPDATEIFCOPY (U): 这个数组是其它数组的一个副本，当这个数组被释放时，原数组的内容将被更新
* ndarray.real: ndarray元素的实部
* ndarray.imag: ndarray 元素的虚部
* ndarray.data: 包含实际数组元素的缓冲区，由于一般通过数组的索引获取元素，所以通常不需要使用这个属性